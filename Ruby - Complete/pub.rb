require 'rubygems'
require 'mqtt'

BROKER = "3.25.240.247"
TOPIC = "/mqtt/ruby"

def connect
  begin
    client = MQTT::Client.connect(BROKER)
    puts "Accessed Skynet"
  rescue
    puts "Unable to access Skynet"
  end
  return client
end

def publish(client, msg)
  client.publish(TOPIC, msg)
  puts "Message published"
end

def init
  puts "Requesting access..."
  client = connect
  msg = ""
  while true
    print "What would you like to send? (Exit:exit) ==> "
    msg = gets.chomp()
    if msg.downcase == "exit"
      client.disconnect
      break
    end
    puts ("Publishing \"" + msg + "\"")
    publish(client, msg)
  end
end

init
