require 'rubygems'
require 'mqtt'

BROKER = "3.25.240.247"
TOPIC = "/mqtt/ruby"

def connect
  begin
    client = MQTT::Client.connect(BROKER)
    puts "Accessed Skynet"
  rescue
    puts "Unable to access Skynet"
  end
  return client
end

def subscribe(client)
  begin
    client.subscribe(TOPIC)
    puts "Subscribed"
    client.get() do |topic, message|
      puts "Transmission Recieved: #{message}"
    end
  rescue
    puts "Error Subscribing"
  end
end

def init
  puts "Requesting access..."
  client = connect
  puts "Subscribing..."
  subscribe(client)
end

init
