from paho.mqtt import client as mqtt_client
import random

broker = "3.25.240.247"
topic = "/mqtt/python"
id = f"python-mqtt-{random.randint(0, 20)}"


def connect_mqtt():
    client = mqtt_client.Client(id)
    client.connect(broker)
    return client


def publish_msg(client, msg):
    result = client.publish(topic, msg)
    status = result[0]
    if status == 0:
        print(f"Transmit successful {msg} on topic {topic}")
    else:
        print("Failed to transmit message")


def subscribe(client: mqtt_client):
    def on_message(client, userdata, msg):
        print(
            f"Recieved transmission: \"{msg.payload.decode()}\" from the topic: \"{msg.topic}\"")
    client.subscribe(topic)
    print("Subscribed!")
    client.on_message = on_message


def __init__():
    print("Attempting to connect...")
    client = connect_mqtt()
    if client.is_connected:
        print("Connected to the Gibson!")
        type = input(
            "What would you like to do (Publish:pub, Subscribe:sub) =>").lower()
        if type == "sub" or type == "subscribe":
            while client.is_connected:
                print("Subscribing...")
                subscribe(client)
                client.loop_forever()
        elif type == "pub" or type == "publish":
            while client.is_connected:
                msg = input(
                    "What would you like to send (Exit:exit)? => ").lower()
                if msg == "exit":
                    client.disconnect()
                    break
                publish_msg(client, msg)


if __name__ == "__main__":
    __init__()
