
print("LOAD: blink.py")


def blinkLED(pin=2):
    from machine import Pin
    import time
    while True:
        p = Pin(pin, Pin.OUT)
        print(f"pin {pin} on")
        p.value(1)
        time.sleep(0.5)
        p.value(0)
        print(f"pin {pin} off")
        time.sleep(0.5)
