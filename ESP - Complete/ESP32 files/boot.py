# This file is executed on every boot of the device

import gc
import esp
import network
import micropython
import machine
import ubinascii
from umqttsimple import MQTTClient
import time
print("LOAD: boot.py")
esp.osdebug(False)
gc.collect()

SSID = 'SSID' # EDIT this line
PASSWORD = 'PASSWORD' # EDIT this line
BROKER = '3.25.240.247' # Edit this line for different broker
CLIENT_ID = ubinascii.hexlify(machine.unique_id())
SUB_TOPIC = '/mqtt/python'

# connect to wifi


def do_connect():
    import network
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print('Connecting to WiFi...')
        wlan.connect(SSID, PASSWORD)
        while not wlan.isconnected():
            pass
    print('Connected WiFi config:', wlan.ifconfig())


do_connect()
