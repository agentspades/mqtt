# This runs after boot

global BROKER, CLIENT_ID, SUB_TOPIC
client = MQTTClient(CLIENT_ID, BROKER)

print("LOAD: main.py")


def rave(pin=2):
    from machine import Pin
    p = Pin(pin, Pin.OUT)
    count = 0
    print("RAAAVVEE")
    while count <= 20:
        p.value(1)
        time.sleep(0.1)
        p.value(0)
        time.sleep(0.1)
        count += 1


def blink(status, pin=2):
    from machine import Pin
    p = Pin(pin, Pin.OUT)
    if status == 1:
        p.value(status)
        print("LED is on")
    elif status == 0:
        p.value(status)
        print("LED is off")
    else:
        print("INVALID OPTION RECIEVED!")


def msg_recieved(topic, msg):
    try:
        if msg.decode('UTF-8') == 'rave' or msg.decode("UTF-8") == 'Rave':
            rave(2)
        else:
            blink(int(msg), 2)
    except:
        print("VALUE MUST BE NUMERICAL")


def connect():
    client.set_callback(msg_recieved)
    print("Connecting...")
    client.connect()
    client.subscribe(SUB_TOPIC)
    print("Connected and Subscribed!")


def restart():
    print("Failed to connect...")
    print("Resetting device to try again")
    time.sleep(5)
    machine.reset()


try:
    connect()
except:
    restart()

print("Waiting for message")

while True:
    client.check_msg()
