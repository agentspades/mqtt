from paho.mqtt import client as mqtt_client
import random

broker = "3.25.240.247"
pub_topic = "/mqtt/python"
sub_topic = "/mqtt/notifications"
id = f"python-mqtt-{random.randint(0, 20)}"


def connect_mqtt():
    client = mqtt_client.Client(id)
    client.connect(broker)
    return client


def publish_msg(client):
    while True:
        usr_input = input(
            "LED on or off (on:1 off:0 Rave:rave Exit:exit) ==> ")
        if usr_input.lower() == 'exit':
            client.disconnect()
            break
        result = client.publish(pub_topic, usr_input)
        status = result[0]
        if usr_input == 'rave' and status == 0:
            print("RAVE!!! DOOF DOOF")
        elif status == 0:
            print(f"Transmit successful")
        else:
            print("Failed to transmit message")


def __init__():
    print("Attempting to connect...")
    client = connect_mqtt()
    publish_msg(client)


if __name__ == "__main__":
    __init__()
