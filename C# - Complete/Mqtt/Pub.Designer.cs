﻿
namespace Mqtt
{
    partial class Pub
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LightSwitch_Btn = new System.Windows.Forms.Button();
            this.Output_TextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // LightSwitch_Btn
            // 
            this.LightSwitch_Btn.Font = new System.Drawing.Font("Copperplate Gothic Bold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LightSwitch_Btn.Location = new System.Drawing.Point(12, 12);
            this.LightSwitch_Btn.Name = "LightSwitch_Btn";
            this.LightSwitch_Btn.Size = new System.Drawing.Size(353, 301);
            this.LightSwitch_Btn.TabIndex = 0;
            this.LightSwitch_Btn.Text = "button1";
            this.LightSwitch_Btn.UseVisualStyleBackColor = true;
            this.LightSwitch_Btn.Click += new System.EventHandler(this.LightSwitch_Btn_Click);
            // 
            // Output_TextBox
            // 
            this.Output_TextBox.BackColor = System.Drawing.SystemColors.InfoText;
            this.Output_TextBox.Font = new System.Drawing.Font("Copperplate Gothic Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Output_TextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.Output_TextBox.Location = new System.Drawing.Point(12, 319);
            this.Output_TextBox.Multiline = true;
            this.Output_TextBox.Name = "Output_TextBox";
            this.Output_TextBox.ReadOnly = true;
            this.Output_TextBox.Size = new System.Drawing.Size(353, 145);
            this.Output_TextBox.TabIndex = 1;
            // 
            // Pub
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 476);
            this.Controls.Add(this.Output_TextBox);
            this.Controls.Add(this.LightSwitch_Btn);
            this.Name = "Pub";
            this.Text = "MQTT";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Pub_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button LightSwitch_Btn;
        private System.Windows.Forms.TextBox Output_TextBox;
    }
}

