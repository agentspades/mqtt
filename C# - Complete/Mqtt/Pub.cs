using System;
using System.Text;
using System.Windows.Forms;
using uPLibrary.Networking.M2Mqtt;

namespace Mqtt
{
    public partial class Pub : Form
    {
        String broker = "3.25.240.247";
        String topic = "/mqtt/csharp";
        MqttClient client = null;

        public Pub()
        {
            InitializeComponent();
        }

        private void Connect()
        {
            Random random = new Random();
            int id = random.Next(0, 200);
            try
            {
                client = new MqttClient(broker, 1883, false, null);
                client.Connect(id.ToString());
                Output_TextBox.AppendText("Sync achieved!" + Environment.NewLine);
            }
            catch (Exception)
            {
                Output_TextBox.Text = "Error syncing!" + Environment.NewLine;
            }
        }

        private void Light(int state)
        {
            //connect to broker if not connected
            if (client == null)
            {
                Output_TextBox.Text = "Preparing for sync..." + Environment.NewLine;
                Output_TextBox.Update();
                Connect();
            }
            if (state == 0)
            {
                Output_TextBox.AppendText("Sending signal" +Environment.NewLine);
                Publish(state.ToString());
            }
            else
            {
                Output_TextBox.Text += "Sending signal" +Environment.NewLine;
                Publish(state.ToString());
            }
        }

        private void Publish(string state)
        {
            byte[] msg;
            if (state.Equals("0"))
            {
                msg = Encoding.ASCII.GetBytes(state);
                client.Publish(topic, msg);
                Output_TextBox.AppendText("Light turned off" +Environment.NewLine);
                LightSwitch_Btn.Text = "On";
            }
            else
            {
                msg = Encoding.ASCII.GetBytes(state);
                client.Publish(topic, msg);
                Output_TextBox.AppendText("Light turned on" +Environment.NewLine);
                LightSwitch_Btn.Text = "Off";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LightSwitch_Btn.Text = "On";
        }

        private void LightSwitch_Btn_Click(object sender, EventArgs e)
        {
            if (LightSwitch_Btn.Text.Equals("On"))
            {
                Light(1);
            }
            else
            {
                Light(0);
            }
        }

        private void Pub_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(client == null)
            {
                this.Dispose();
            }
            else if (client.IsConnected)
            {
                client.Disconnect();
            }
        }
    }
}
