using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace Mqtt_sub
{
    public partial class Sub : Form
    {
        string broker = "3.25.240.247";
        string[] topic = {"/mqtt/csharp"};
        MqttClient client = null;
        public Sub()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Light_Panel.BackColor = Color.Black;
        }

        private void Sync_Btn_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            int id = random.Next(200, 400);
            Output_TextBox.Text = "Preparing for sync..." + Environment.NewLine;
            Output_TextBox.Update();
            try
            {
                client = new MqttClient(broker, 1883, false, null);
                client.Connect(id.ToString());
                Output_TextBox.Text += "Sync achieved!" + Environment.NewLine;
                Output_TextBox.Text += "Subscribing ..." +Environment.NewLine;
                Subscribe();
                client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
            }
            catch (Exception)
            {
                Output_TextBox.Text += "Error syncing!" + Environment.NewLine;
            }
        }

        void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            string msgRecieved = Encoding.UTF8.GetString(e.Message);
            if (msgRecieved.Equals("1"))
            {
                Light_Panel.BackColor = Color.White;
            }
            else
            {
                Light_Panel.BackColor = Color.Black;
            }
        }

        private void Subscribe()
        {
            byte[] qos = { 1 };
            try
            {
                client.Subscribe(topic, qos);
                Output_TextBox.Text += "Subscribed!" + Environment.NewLine;
            }
            catch (Exception)
            {
                Output_TextBox.Text += "Unable to subscribe" +Environment.NewLine;
            }
        }

        private void Sub_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(client == null)
            {
                this.Dispose();
            }
            else if (client.IsConnected)
            {
                client.Disconnect();
            }
        }
    }
}
