﻿
namespace Mqtt_sub
{
    partial class Sub
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Light_Panel = new System.Windows.Forms.Panel();
            this.Sync_Btn = new System.Windows.Forms.Button();
            this.Output_TextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Light_Panel
            // 
            this.Light_Panel.Location = new System.Drawing.Point(12, 12);
            this.Light_Panel.Name = "Light_Panel";
            this.Light_Panel.Size = new System.Drawing.Size(385, 250);
            this.Light_Panel.TabIndex = 0;
            // 
            // Sync_Btn
            // 
            this.Sync_Btn.Font = new System.Drawing.Font("Copperplate Gothic Bold", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sync_Btn.Location = new System.Drawing.Point(12, 268);
            this.Sync_Btn.Name = "Sync_Btn";
            this.Sync_Btn.Size = new System.Drawing.Size(385, 51);
            this.Sync_Btn.TabIndex = 1;
            this.Sync_Btn.Text = "Sync";
            this.Sync_Btn.UseVisualStyleBackColor = true;
            this.Sync_Btn.Click += new System.EventHandler(this.Sync_Btn_Click);
            // 
            // Output_TextBox
            // 
            this.Output_TextBox.BackColor = System.Drawing.SystemColors.InfoText;
            this.Output_TextBox.Font = new System.Drawing.Font("Copperplate Gothic Light", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Output_TextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.Output_TextBox.Location = new System.Drawing.Point(12, 325);
            this.Output_TextBox.Multiline = true;
            this.Output_TextBox.Name = "Output_TextBox";
            this.Output_TextBox.Size = new System.Drawing.Size(385, 104);
            this.Output_TextBox.TabIndex = 2;
            // 
            // Sub
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 442);
            this.Controls.Add(this.Output_TextBox);
            this.Controls.Add(this.Sync_Btn);
            this.Controls.Add(this.Light_Panel);
            this.Name = "Sub";
            this.Text = "MQTT";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Sub_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Light_Panel;
        private System.Windows.Forms.Button Sync_Btn;
        private System.Windows.Forms.TextBox Output_TextBox;
    }
}

