package com.agentspades.mqtt_pub;

import java.util.Scanner;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class Main {
    // Global variables
    static String broker = "tcp://3.25.240.247:1883";
    static String topic = "/mqtt/java";
    static String id = MqttClient.generateClientId();
    static MemoryPersistence persistence = new MemoryPersistence();
    
    private static MqttClient ConnectToBroker(){
        MqttClient client = null;
        try{
            client = new MqttClient(broker, id, persistence);
            MqttConnectOptions options = new MqttConnectOptions();
            options.setCleanSession(true);
            options.setConnectionTimeout(10);
            System.out.println("Connecting to the mainframe on : " +broker);
            client.connect(options);
            System.out.println("Uplink achieved " +broker);
        }
        catch(MqttException me){
            System.out.println("There was an error connecting to the mainframe");
        }
        return client;
    }
    
    private static void Publish(MqttClient client){
        String input;
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.print("What message would you like to send? (Exit:exit) -> ");
            input = sc.nextLine();
            if (input.toLowerCase().equals("exit")){
                try{
                    client.disconnect();
                    System.out.println("Disconnected from the mainframe");
                }
                catch(Exception e){
                    System.out.println("Error disconnecting from the mainframe");
                }
                break;
            }
            System.out.println("Attempting to publish \"" +input+"\"");
            MqttMessage msg = new MqttMessage(input.getBytes());
            try{
                client.publish(topic, msg);
                System.out.println("Message published!");
            }
            catch(MqttException me){
                System.out.println("There was an error publishing your message!");
            }
        }
    }
    
    public static void main(String args[]) {
        System.out.println(" /$$      /$$  /$$$$$$  /$$$$$$$$ /$$$$$$$$       /$$$$$$$  /$$   /$$ /$$$$$$$  /$$       /$$$$$$  /$$$$$$  /$$   /$$\n" +
                            "| $$$    /$$$ /$$__  $$|__  $$__/|__  $$__/      | $$__  $$| $$  | $$| $$__  $$| $$      |_  $$_/ /$$__  $$| $$  | $$\n" +
                            "| $$$$  /$$$$| $$  \\ $$   | $$      | $$         | $$  \\ $$| $$  | $$| $$  \\ $$| $$        | $$  | $$  \\__/| $$  | $$\n" +
                            "| $$ $$/$$ $$| $$  | $$   | $$      | $$         | $$$$$$$/| $$  | $$| $$$$$$$ | $$        | $$  |  $$$$$$ | $$$$$$$$\n" +
                            "| $$  $$$| $$| $$  | $$   | $$      | $$         | $$____/ | $$  | $$| $$__  $$| $$        | $$   \\____  $$| $$__  $$\n" +
                            "| $$\\  $ | $$| $$/$$ $$   | $$      | $$         | $$      | $$  | $$| $$  \\ $$| $$        | $$   /$$  \\ $$| $$  | $$\n" +
                            "| $$ \\/  | $$|  $$$$$$/   | $$      | $$         | $$      |  $$$$$$/| $$$$$$$/| $$$$$$$$ /$$$$$$|  $$$$$$/| $$  | $$\n" +
                            "|__/     |__/ \\____ $$$   |__/      |__/         |__/       \\______/ |_______/ |________/|______/ \\______/ |__/  |__/");
        System.out.println();
        MqttClient client = ConnectToBroker();
        Publish(client);
    }
}
