package com.agentspades.mqtt_sub;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class Main {
    // Global variables
    static String broker = "tcp://3.25.240.247:1883";
    static String topic = "/mqtt/java";
    static String id = MqttClient.generateClientId();
    static MemoryPersistence persistence = new MemoryPersistence();
    
    private static MqttClient ConnectToBroker(){
        MqttClient client = null;
        try{
            client = new MqttClient(broker, id, persistence);
            MqttConnectOptions options = new MqttConnectOptions();
            options.setCleanSession(true);
            options.setConnectionTimeout(10);
            System.out.println("Connecting to the mainframe on : " +broker);
            client.connect(options);
            System.out.println("Uplink achieved " +broker);
        }
        catch(MqttException me){
            System.out.println("There was an error connecting to the mainframe");
        }
        return client;
    }
    
    private static void Subscribe(MqttClient client){
        System.out.println("Subscribing to \"" +topic+ "\"");
        try{
            client.subscribe(topic, new IMqttMessageListener(){
                @Override
                public void messageArrived(String topic, MqttMessage message){
                    System.out.println("Message recieved: " +message);
                }
            });
            System.out.println("Successfully subscribed");
        }
        catch(Exception me){
            System.out.println("There was an error subscribing to \"" +topic+ "\"");
        }
    }
    
    public static void main(String args[]) {
        System.out.println(" /$$      /$$  /$$$$$$  /$$$$$$$$ /$$$$$$$$        /$$$$$$  /$$   /$$ /$$$$$$$   /$$$$$$   /$$$$$$  /$$$$$$$  /$$$$$$ /$$$$$$$  /$$$$$$$$\n" +
                            "| $$$    /$$$ /$$__  $$|__  $$__/|__  $$__/       /$$__  $$| $$  | $$| $$__  $$ /$$__  $$ /$$__  $$| $$__  $$|_  $$_/| $$__  $$| $$_____/\n" +
                            "| $$$$  /$$$$| $$  \\ $$   | $$      | $$         | $$  \\__/| $$  | $$| $$  \\ $$| $$  \\__/| $$  \\__/| $$  \\ $$  | $$  | $$  \\ $$| $$      \n" +
                            "| $$ $$/$$ $$| $$  | $$   | $$      | $$         |  $$$$$$ | $$  | $$| $$$$$$$ |  $$$$$$ | $$      | $$$$$$$/  | $$  | $$$$$$$ | $$$$$   \n" +
                            "| $$  $$$| $$| $$  | $$   | $$      | $$          \\____  $$| $$  | $$| $$__  $$ \\____  $$| $$      | $$__  $$  | $$  | $$__  $$| $$__/   \n" +
                            "| $$\\  $ | $$| $$/$$ $$   | $$      | $$          /$$  \\ $$| $$  | $$| $$  \\ $$ /$$  \\ $$| $$    $$| $$  \\ $$  | $$  | $$  \\ $$| $$      \n" +
                            "| $$ \\/  | $$|  $$$$$$/   | $$      | $$         |  $$$$$$/|  $$$$$$/| $$$$$$$/|  $$$$$$/|  $$$$$$/| $$  | $$ /$$$$$$| $$$$$$$/| $$$$$$$$\n" +
                            "|__/     |__/ \\____ $$$   |__/      |__/          \\______/  \\______/ |_______/  \\______/  \\______/ |__/  |__/|______/|_______/ |________/");
        System.out.println();
        MqttClient client = ConnectToBroker();
        Subscribe(client);
    }
}
